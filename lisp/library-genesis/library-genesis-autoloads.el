;;; library-genesis-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "library-genesis" "library-genesis.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from library-genesis.el

(autoload 'library-genesis-search "library-genesis" "\
Search Library Genesis. Interactively, prompt for a search
type and query.
If called from lisp, TYPE must be an element in
`library-genesis-search-types', and QUERY must be a string.

\(fn TYPE QUERY)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "library-genesis" '("library-genesis-")))

;;;***

(provide 'library-genesis-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; library-genesis-autoloads.el ends here
